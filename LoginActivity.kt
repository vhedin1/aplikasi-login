package com.example.aplikasiandroid

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val btnRegistrasi = findViewById<Button>(R.id.btnRegistrasi)

        btnRegistrasi.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}